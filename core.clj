(ns lab2.core
  (:gen-class))

(require '[clojure.core.async :as async :refer [chan go go-loop <! >! >!! <!!]])

(defn vector-chans[size]
  (if (= size 0)
    (vector)
    (conj (vector-chans(- size 1)) (chan))))

(defn worker [number delimiter channels]
   (go (>! (get channels (rem number delimiter)) number)
       )
  )

(defn split-channel-by-delimiter
  [input-chanel delimiter]
  (let [channels (vector-chans delimiter)]
    (go-loop []
      (println "go-loop")
      (let [number (<! input-chanel)]
        (worker number delimiter channels)
        (recur)))
    channels
    )
  )

(defonce number-chan (chan))

(def chans (split-channel-by-delimiter number-chan 3))

(defn -main
  [& args]
  )
  ;(println "Started well")